import lcm
import time
import numpy as np
import sys

from lcmtypes import dynamixel_command_t
from lcmtypes import dynamixel_command_list_t
from lcmtypes import dynamixel_status_t
from lcmtypes import dynamixel_status_list_t

PI = np.pi
D2R = PI/180.0
R2D = 180.0/PI
ANGLE_TOL = 5*PI/180.0 
MIN_SPEED = 0.01162 * 1
SPEED_FACTOR = 1/(1024.0 * 0.01162)
ARM_LENGTH = [116.0 , 100.0, 100.0, 109.0]
MIN_RADIUS = 100.0

""" Rexarm Class """                             
class Rexarm():
    def __init__(self):

        """ Commanded Values """
        self.joint_angles = [0.0, 0.0, 0.0, 0.0] # radians

        self.speed = [0.5,0.5,0.5,0.5]           # 0 to 1

        # you SHOULD change this to contorl each joint speed separately 


        self.max_torque = 0.5                    # 0 to 1
                                   
        """ Feedback Values """
        self.joint_angles_fb = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_fb = [0.0, 0.0, 0.0, 0.0]        # 0 to 1   
        self.load_fb = [0.0, 0.0, 0.0, 0.0]         # -1 to 1  
        self.temp_fb = [0.0, 0.0, 0.0, 0.0]         # Celsius               

        """ Plan - TO BE USED LATER """
        self.plan = []
        self.plan_status = 0
        self.wpt_number = 0
        self.wpt_total = 0
        
        
        """Joint Tolerance"""
        self.jointAngTolerance = np.deg2rad(3.0)

        """ LCM Stuff"""
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("ARM_STATUS",
                                        self.feedback_handler)

        """Flag for cubic smoothing"""
        self.cubic = 0
        self.straight = 0
        self.oldTime = 0.0
        
        
        """DH parameter and world coordinates"""
        self.dh_table = [[PI/2,-PI/2, 0, 0],[ARM_LENGTH[0],0.0,0.0,0.0],[0.0,ARM_LENGTH[1],ARM_LENGTH[2],ARM_LENGTH[3]],\
        [-PI/2,0.0,0.0,0.0]]
        self.worldC=[0.0,0.0,0.0,0.0]
        
    def cmd_publish(self):
        """ 
        Publish the commands to the arm using LCM. 
        NO NEED TO CHANGE.
        You need to activelly call this function to command the arm.
        You can uncomment the print statement to check commanded values.
        """    
        msg = dynamixel_command_list_t()
        msg.len = 4
        self.clamp()
        for i in range(msg.len):
            cmd = dynamixel_command_t()
            cmd.utime = int(time.time() * 1e6)
            cmd.position_radians = self.joint_angles[i]

            cmd.speed = self.speed[i]

            # you SHOULD change this to contorl each joint speed separately 


            cmd.max_torque = self.max_torque
            #print cmd.position_radians
            msg.commands.append(cmd)
        self.lc.publish("ARM_COMMAND",msg.encode())
    
    def get_feedback(self):
        """
        LCM Handler function
        Called continuously from the GUI 
        NO NEED TO CHANGE
        """
        self.lc.handle_timeout(50)

    def feedback_handler(self, channel, data):
        """
        Feedback Handler for LCM
        NO NEED TO CHANGE FOR NOW.
        LATER NEED TO CHANGE TO MANAGE PLAYBACK FUNCTION 
        """
        msg = dynamixel_status_list_t.decode(data)
        for i in range(msg.len):
            self.joint_angles_fb[i] = msg.statuses[i].position_radians 
            self.speed_fb[i] = msg.statuses[i].speed 
            self.load_fb[i] = msg.statuses[i].load 
            self.temp_fb[i] = msg.statuses[i].temperature

    def clamp(self):
        """
        Clamp Function
        Limit the commanded joint angles to ones physically possible so the 
        arm is not damaged.
        TO DO: IMPLEMENT SUCH FUNCTION
        """
        self.joint_angles[0] = (self.joint_angles[0] + PI) % (2 * PI)- PI
        
        if(self.joint_angles[1] < -123.0*D2R):
            self.joint_angles[1] = -123.0*D2R
            print'Shoulder angle out of range'
            #sys.exit(0)
        if (self.joint_angles[1] > 121.0*D2R):
            self.joint_angles[1] = 121.0*D2R
            print'Shoulder angle out of range'
            #sys.exit(0)
        if(self.joint_angles[2] < -123.0*D2R):
            self.joint_angles[2] = -123.0*D2R
            print'Elbow angle out of range'
            #sys.exit(0)
        if (self.joint_angles[2] > 121.0*D2R):
            self.joint_angles[2] = 121.0*D2R
            print'Elbow angle out of range'
            #sys.exit(0)
        if(self.joint_angles[3] < -127.0*D2R):
            self.joint_angles[3] = -127.0*D2R
            print'Wrist angle out of range'
            #sys.exit(0)
        if (self.joint_angles[3] > 127.0*D2R):
            self.joint_angles[3] = 127.0*D2R
            print'Wrist angle out of range'
            #sys.exit(0)
        
                
        pass

    def plan_command(self):
        """ Command waypoints - TO BE ADDED LATER """
        if(self.plan_status):
            print "Waypoint:",self.wpt_number
            self.joint_angles = self.plan[self.wpt_number][1]
            self.speed = np.array([0.2]*4)
            
            if(self.cubic == 1):
                if(self.wpt_number < self.wpt_total and self.wpt_number > 0):
                    last = self.plan[self.wpt_number - 1]
		    next = self.plan[self.wpt_number]
		    t0 = 0
		    tf = next[0] - last[0]
                    if(self.wpt_number % 3 == 0):
		        
		        A = np.array([[1,0,0,0],[0,1,0,0],[1,tf,tf**2,tf**3],[0,1,2*tf,3*tf**2]])
                        Q = np.vstack((last[1],[0,0,0,0],next[1],[0,0,0,0]))
                        B = np.dot(np.linalg.inv(A),Q)
		    
                        dT = time.time() - self.oldTime
		        if(dT-tf > 0.1):
			    dT = tf
			    print 'Waypoint was not reached in time'
		    
		        T = np.array([1, dT, dT**2, dT**3])
		        Tv = np.array([0, 1, 2*dT, 3*dT**2])
		        angles = np.dot(T,B).tolist()
		        speed = SPEED_FACTOR * np.dot(Tv,B)
		    
		        self.joint_angles = angles
		        self.speed = speed
		        
		        #print B
                        #print "Tf, dT: ", tf, dT
                    else:
                        self.joint_angles = self.plan[self.wpt_number][1]
                        self.speed = SPEED_FACTOR * np.abs(np.array(next[1]) - np.array(last[1]))/tf
                    
            elif(self.straight == 1):
                if(self.wpt_number < self.wpt_total and self.wpt_number > 0):
                    last = self.plan[self.wpt_number - 1]
                    next = self.plan[self.wpt_number]
                    last_global = np.array(self.rexarm_FK2(self.dh_table,4,last[1]))
                    next_global = np.array(self.rexarm_FK2(self.dh_table,4,next[1]))
            	    tf = next[0] - last[0]
            	    
                    if (self.wpt_number % 3 == 0):
                        last_xy = last_global[0:2]
                        next_xy = next_global[0:2]
                        print 'last:',last_xy
                        print 'next:',next_xy
                        distance = MIN_RADIUS
                        d_dist = np.array([0,0])
                        if (np.dot(next_xy,next_xy - last_xy) > 0 and np.dot(last_xy,last_xy - next_xy) > 0):
                            r_1 = np.linalg.norm(last_xy)
                            r_2 = np.linalg.norm(next_xy)
                            l = np.linalg.norm(last_xy - next_xy)
                            ang_r_1 = np.arccos((r_2**2 + l**2 - r_1**2)/(2*r_2*l))
                            distance = np.sin(ang_r_1) * r_2
                            k = np.cos(ang_r_1)*r_2/l
                            d_dist = k * last_xy + (1 - k)* next_xy
                            d_dist = d_dist/np.linalg.norm(d_dist)
                        
            	        dxyzphi = next_global - last_global
            	        
            	        dT = time.time() - self.oldTime
            	        
            	        if(dT-tf > 0.1):
                            dT = tf
                            print 'Waypoint was not reached in time'
            	        
            	        current_global = last_global + dT/tf * dxyzphi
            	        print 'current_global',current_global
            	        if (np.linalg.norm(current_global[0:2]) < MIN_RADIUS):
            	            current_xy = current_global[0:2]
            	            h = np.sqrt(MIN_RADIUS**2 - (np.linalg.norm(current_xy))**2 + distance**2) - distance
            	            current_global[0:2] = current_xy + h * d_dist
            	        
            	        
            	        
            	        angles = self.rexarm_IK(current_global,1)
            	        self.joint_angles = angles

            	    else:
            	        self.joint_angles = self.plan[self.wpt_number][1]
            	        self.speed = SPEED_FACTOR * np.abs(np.array(next[1]) - np.array(last[1]))/tf

            elif(self.cubic == 0 and self.straight == 0):
                if (self.wpt_number >= 1):
                    self.joint_angles = self.plan[self.wpt_number][1]
                    dtime = self.plan[self.wpt_number][0]-self.plan[self.wpt_number-1][0]
                    "Teaching speed"
                    self.speed =SPEED_FACTOR * np.abs(np.array(self.plan[self.wpt_number][1]) - np.array(self.plan[self.wpt_number-1][1]))/dtime
                        
                    "Constant speed"
                    #self.speed = np.array([0.2]*4)
                    
                    #print "set speed :", self.speed
            
            print "Speed:",self.speed
            #print self.wpt_number
            #print self.plan
            error =np.abs( np.array(self.plan[self.wpt_number][1]) - np.array(self.joint_angles_fb))
            self.speed = np.array(2*np.abs(self.speed))
            self.speed[self.speed < 3*MIN_SPEED] = 3*MIN_SPEED
            self.speed[error < ANGLE_TOL] = MIN_SPEED 
            
            
            
            """save file"""
            fGraph = open('way_points_graph.txt','a')
            t_graph = time.time()-self.oldTime
            fGraph.write(str(self.joint_angles[0])+','+str(self.joint_angles[1])+','+\
            	    str(self.joint_angles[2])+','+str(self.joint_angles[3])+','+\
            	    str(self.joint_angles_fb[0])+','+str(self.joint_angles_fb[1])+','+\
            	    str(self.joint_angles_fb[2])+','+str(self.joint_angles_fb[3])+','+str(t_graph)+'\n')
            fGraph.close()
            fGraph_vel = open('way_points_vel.txt','a')
            fGraph_vel.write(str(self.speed[0])+','+str(self.speed[1])+','+str(self.speed[2])+','+str(self.speed[3])+','+\
            	    str(self.speed_fb[0])+','+str(self.speed_fb[1])+','+str(self.speed_fb[2])+','+str(self.speed_fb[3])+','+ str(t_graph)+'\n')
            fGraph_vel.close()
            end_pos = self.rexarm_FK2(self.dh_table,4,self.joint_angles)
            fGraph_end = open('way_points_end.txt','a')
            fGraph_end.write(str(end_pos[0])+','+str(end_pos[1])+','+str(end_pos[2])+','+\
            	    str(self.worldC[0])+','+str(self.worldC[1])+','+str(self.worldC[2])+ ',' + str(t_graph)+'\n')
            fGraph_end.close()
            
            
           
            
            #print ("joint angles = ",self.joint_angles)
            #print ("joint angles fb = ",self.joint_angles_fb)
            #print ("error = "+str(error))
            if (np.all(error < ANGLE_TOL)):
                self.wpt_number += 1
                self.oldTime = time.time()
            if(self.wpt_number==self.wpt_total):
                self.plan_status = 0
                self.wpt_number = 0
                self.speed = np.array([0.2]*4)
            
            #print "Angles:",self.joint_angles
            #print "Clamped Speed:", self.speed
            print np.all(error < ANGLE_TOL)
            self.speed = self.speed.tolist()
            
            self.cmd_publish()
    def rexarm_FK(self, dh_table, link):
        """
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for
        returns a 4-tuple (x, y, z, phi) representing the pose of the 
        desired link
        """
        theta=np.array(dh_table[0])+np.array(self.joint_angles_fb)
        d = dh_table[1]
        a = dh_table[2]
        alpha = dh_table[3]

        T = np.eye(4)
        for i in range (link):
            mat = np.array([[np.cos(theta[i]),-np.sin(theta[i])*np.cos(alpha[i]), np.sin(theta[i])*np.sin(alpha[i]), a[i]*np.cos(theta[i])],\
            [np.sin(theta[i]),np.cos(theta[i])*np.cos(alpha[i]),-np.cos(theta[i])*np.sin(alpha[i]),a[i]*np.sin(theta[i])],\
            [0.0, np.sin(alpha[i]),np.cos(alpha[i]),d[i]],\
            [0.0,0.0,0.0,1.0]])
            T = np.dot(T,mat)
        self.worldC[0] = T[0][3]
        self.worldC[1] = T[1][3]
        self.worldC[2] = T[2][3]
        phi = np.abs(self.joint_angles_fb[1]+self.joint_angles_fb[2]+self.joint_angles_fb[3]) - PI/2
        self.worldC[3] = phi*R2D
        #print self.worldC
        
        pass

    def rexarm_FK2(self, dh_table, link, angles):
        """
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for
        returns a 4-tuple (x, y, z, phi) representing the pose of the 
        desired link
        """
        theta=np.array(dh_table[0])+np.array(angles)
        d = dh_table[1]
        a = dh_table[2]
        alpha = dh_table[3]

        T = np.eye(4)
        for i in range (link):
            mat = np.array([[np.cos(theta[i]),-np.sin(theta[i])*np.cos(alpha[i]), np.sin(theta[i])*np.sin(alpha[i]), a[i]*np.cos(theta[i])],\
            [np.sin(theta[i]),np.cos(theta[i])*np.cos(alpha[i]),-np.cos(theta[i])*np.sin(alpha[i]),a[i]*np.sin(theta[i])],\
            [0.0, np.sin(alpha[i]),np.cos(alpha[i]),d[i]],\
            [0.0,0.0,0.0,1.0]])
            T = np.dot(T,mat)
        #self.worldC[0] = T[0][3]
        #self.worldC[1] = T[1][3]
        #self.worldC[2] = T[2][3]
        phi = np.abs(angles[1]+angles[2]+angles[3]) - PI/2
        #self.worldC[3] = phi*R2D
        
        return [T[0][3],T[1][3],T[2][3],phi*R2D]
        #print self.worldC
        
        pass
        
    def rexarm_IK(self, pose, cfg):
        """
        Calculates inverse kinematics for the rexarm
        pose is a tuple (x, y, z, phi) which describes the desired
        end effector position and orientation.  
        cfg describe elbow down (0) or elbow up (1) configuration
        returns a 4-tuple of joint angles or NONE if configuration is impossible
        """
        base_angle = -np.arctan2(pose[0],pose[1])
        base_pose = np.array([0.0,0.0,ARM_LENGTH[0]])
        #Cz = np.array([[ 1, cos(base_angle), sin(base_angle)],[-sin(base_angle), cos(base_angle), 0],[0, 0, 1]])
        #C2 = np.array([[ cos(base_angle), 0, -sin(base_angle)],[0,1,0],[sin(base_angle), 0, cos(base_angle)]])
        #dir_vector = [0,0,1]
        #dir_vector = np.array([-np.sin(base_angle),np.cos(base_angle),-np.sin(pose[3])])
        #wrist_pose = np.array(pose[0:3]) - ARM_LENGTH[3]*dir_vector/np.linalg.norm(dir_vector)
        R = np.sqrt(pose[0]**2 + pose[1]**2);
        b = R - ARM_LENGTH[3]*np.cos(D2R*pose[3])
        y = b*np.cos(base_angle)
        x = -b*np.sin(base_angle)
        z = pose[2] + ARM_LENGTH[3]*np.sin(D2R*pose[3])
        wrist_pose = np.array([x,y,z])
        l = np.linalg.norm(base_pose - wrist_pose)
        if l > ARM_LENGTH[1]+ARM_LENGTH[2]:
            print D2R*pose[3]
            print base_pose, wrist_pose, l
            print 'target out of range'
            return None

        elbow_angle = PI-np.arccos((ARM_LENGTH[1]**2 + ARM_LENGTH[2]**2 - l**2)/(2*ARM_LENGTH[1]*ARM_LENGTH[2]))
        arm2_angle = np.arccos((ARM_LENGTH[1]**2 + l**2 - ARM_LENGTH[2]**2)/(2*ARM_LENGTH[1]*l))
        l_hori_angle = np.arcsin((wrist_pose[2]-base_pose[2])/l)
        
        if cfg == 1:
            shoulder_angle = PI/2 -l_hori_angle - arm2_angle
        elif cfg == 0:
            elbow_angle = -elbow_angle
            shoulder_angle = PI/2 + arm2_angle - l_hori_angle
        else:
            print 'config out of range'
            return None
        
        wrist_angle = D2R*pose[3] + PI/2 - shoulder_angle - elbow_angle
        
        return [base_angle,shoulder_angle,elbow_angle,wrist_angle]
        
        
        pass
        
    def rexarm_collision_check(q):
        """
        Perform a collision check with the ground and the base
        takes a 4-tuple of joint angles q
        returns true if no collision occurs
        """
        pass 
