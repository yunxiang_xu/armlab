import sys
import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm
from copy import deepcopy
from video import Video
import time
import Image
#from scipy.signal import butter, lfilter, freqz

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510


class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))
        
        
        
        

        """ Other Variables """
        self.last_click = np.float32([0,0])
        

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """ 
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)
       
        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()
        
        self.plantimer = QtCore.QTimer(self)
        self.plantimer.timeout.connect(self.rex.plan_command)
        self.plantimer.start()
        
        self.fktimer = QtCore.QTimer(self)
        fcn = lambda : self.rex.rexarm_FK(self.rex.dh_table,4)
        self.fktimer.timeout.connect(fcn)
        self.fktimer.start()
        
        self.exectimer = QtCore.QTimer(self)
        self.exectimer.timeout.connect(self.exec_path_act)
        self.exectimer.start(27)

        """ 
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI 
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)
        
        self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)
        self.ui.sldrSpeed.valueChanged.connect(self.slider_change)

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)

        self.camCalibrated = 0
        self.t_0 = 0.0
        self.exec_status = False
        self.straightLine = 0
        
        self.template_flag = 0
        self.template_coord =np.float32([[0.0,0.0],[0.0,0.0]])
        self.mouse_id = 0
        self.template = []
        self.locations = np.array([])
        
        "Number of points for interpolation"
        self.Fs = 20

    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """

        """ Renders the Video Frame """
        try:
            self.video.captureNextFrame()
            self.ui.videoFrame.setPixmap(
                self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)
        except TypeError:
            print "No frame"
        
        """ 
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels 
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))
        
        
        self.ui.rdoutX.setText(str(self.rex.worldC[0]))
        self.ui.rdoutY.setText(str(self.rex.worldC[1]))
        self.ui.rdoutZ.setText(str(self.rex.worldC[2]))
        self.ui.rdoutT.setText(str(self.rex.worldC[3]))
        
        """ 
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates 
        """    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
                """ TO DO Here is where affine calibration must be used """
                mousePix=np.array([x,y,1]).transpose()
                mouseWorld = np.dot(self.video.aff_matrix,mousePix)
                x = mouseWorld[0]
                y = mouseWorld[1]
                self.ui.rdoutMouseWorld.setText("(%.0f,%.0f)" % (x,y))
                
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))
    

    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        TO DO: Implement for the other sliders
        """
        self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
        self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
        self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
        self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
        self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
        self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()) + "%")
        self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
        self.rex.speed = 4 * [self.ui.sldrSpeed.value()/100.0]
        if (self.rex.plan_status == 0):
            self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
            self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
            self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
            self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R
        self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration 
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): return

        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y
       
        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """ 
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0
                
                """ Perform affine calibration with OpenCV """
                #self.video.aff_matrix = cv2.getAffineTransform(
                #                        self.video.mouse_coord,
                #                        self.video.real_coord)
                
                b = np.ones((1,5))
                P = self.video.mouse_coord.transpose()
                P = np.vstack((P,b))
                W = self.video.real_coord.transpose()
                W = np.vstack((W,b))
                
                self.video.aff_matrix = np.dot(W,np.linalg.pinv(P))
                """ Updates Status Label to inform calibration is done """ 
                self.ui.rdoutStatus.setText("Waiting for input")

                """ 
                Uncomment to gether affine calibration matrix numbers 
                on terminal
                """ 
                print self.video.aff_matrix
        
        elif (self.template_flag == 1):
            self.template_coord[self.mouse_id] = [(x-MIN_X),(y-MIN_Y)]
            self.mouse_id += 1
            
            if(self.mouse_id > 1):
            	print self.template_coord
            	
            	x = 2 * np.sort([self.template_coord[0][0], self.template_coord[1][0]])
            	y = 2 * np.sort([self.template_coord[0][1], self.template_coord[1][1]])
            	
                        
                x_slice = slice(x[0],x[1])
                y_slice = slice(y[0],y[1])
                	
            	self.template = self.video.currentFrame[y_slice,x_slice]
            	
            	img = Image.fromarray(self.template)
            	
            	print np.shape(self.video.currentFrame)
            	
            	img.save("template.jpg")
            	#print self.template
            	
            	self.template_flag = 0
            	
        	
        
    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status text label 
        """
        self.video.aff_flag = 1 
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        self.video.loadCalibration()
               
        print "Load Camera Cal"                                                                             

    def tr_initialize(self):
    	self.rex.plan = []
    	self.rex.wpt_total = 0
    	self.rex.plan_status = 0
        print "Teach and Repeat"

    def tr_add_waypoint(self):
    	#print 'fb angles', self.rex.joint_angles_fb
    	#print 'pre-existing plan', self.rex.plan
    	desired_angles = deepcopy(self.rex.joint_angles_fb)
    	self.rex.plan.append([time.time(),desired_angles])
    	self.rex.wpt_total += 1
        print "Add Waypoint "
        print 'plan after append',self.rex.plan

    def tr_smooth_path(self):
    	temp = []
    	offset = 0.5
    	total_offset = 0
    	for i in range(self.rex.wpt_total):
            current = self.rex.plan[i]
    	    top = deepcopy(current)
    	    #top[1][1] = top[1][1] - np.sign(top[1][1]) * 15 * D2R
    	    
    	    
    	    pos = self.rex.rexarm_FK2(self.rex.dh_table,4,top[1])
    	    print pos
    	    top[1] = self.rex.rexarm_IK([pos[0],pos[1],pos[2] + 40, pos[3]],1)
    	    print top[1]
    	    
    	    top2 = deepcopy(top)
    	    top[0] = current[0] + total_offset
    	    current[0] = current[0] + offset + total_offset
    	    top2[0] = current[0] + offset
    	    total_offset = total_offset + 2*offset
    	    temp.append(top)
    	    temp.append(current)
    	    temp.append(top2)
    	    
    	self.rex.plan = temp
    	self.rex.wpt_total = len(temp)
        if (self.straightLine == 1):
            temp = []
            nb_points = 50
            for i in range(self.rex.wpt_total - 1):
            	last_wpt = self.rex.plan[i]
            	next_wpt = self.rex.plan[i + 1]
            	last_global = self.rex.rexarm_FK2(self.rex.dh_table,4,last_wpt[1])
            	next_global = self.rex.rexarm_FK2(self.rex.dh_table,4,next_wpt[1])
            	last_time = last_wpt[0]
            	next_time = next_wpt[0]
            	timesteps = np.linspace(last_time, next_time,nb_points) 
            	x_points = np.linspace(last_global[0],next_global[0],nb_points)
            	y_points = np.linspace(last_global[1],next_global[1],nb_points)
            	z_points = np.linspace(last_global[2],next_global[2],nb_points)
            	phi_points = np.linspace(last_global[3],next_global[3],nb_points)
            	for j in range(len(timesteps)):
            	    joint_angles = self.rex.rexarm_IK([x_points[j],y_points[j],z_points[j],phi_points[j]],1)
            	    t = timesteps[j]                                           
            	    temp.append([t,joint_angles])
            self.rex.plan = temp
            self.rex.wpt_total = len(temp)
            	    	    
    	"""
    	#joint_paths = zip(*zip(*temp)[1])
    	temp = [[],[],[],[],[]]
    	for i in range(self.rex.wpt_total - 1):
    	    deltaT = self.rex.plan[i+1][0] - self.rex.plan[i][0]
            temp[0].extend(np.linspace(self.rex.plan[i][0],self.rex.plan[i+1][0], np.round(self.Fs * deltaT)).tolist())
            for k in range(4): 
                temp[k+1].extend(np.linspace(self.rex.plan[i][1][k],self.rex.plan[i+1][1][k], np.round(self.Fs * deltaT)).tolist())
        
        
        cutoff = 10000
        print(temp)
        b, a = butter(5, cutoff/(0.5*self.Fs), btype = 'low', analog = False)
        for i in range(4):
            print np.shape(temp[i+1])
            y = lfilter(b, a, np.array(temp[i+1]))
            temp[i+1] = y.tolist()
        print len(temp), len(temp[0])
    	joint_list = [list(a) for a in zip(temp[1],temp[2],temp[3],temp[4])]
    	self.rex.plan = [list(a) for a in zip(temp[0],joint_list)]
    	print self.rex.plan[1]
    	"""
    	print "Times: ", np.array(zip(*self.rex.plan)[0]) - zip(*self.rex.plan)[0][0]
    	print "Plan: ", self.rex.plan
    	#sys.exit(0)
    	
        print "Smooth Path"

    def tr_playback(self):
    	wpt_total = deepcopy(self.rex.wpt_total)
    	
    	
    	"""save file"""
    	
        fPlan = open('way_points_plan.txt','a')
    	fPlan.write('wpt_total'+':'+str(self.rex.wpt_total)+'\n')
    	for i in range(wpt_total):
            
    	    end_pos = self.rex.rexarm_FK2(self.rex.dh_table,4,self.rex.plan[i][1])
    	    t_plan = self.rex.plan[i][0]
            fPlan.write(str(self.rex.plan[i][1][0])+','+str(self.rex.plan[i][1][1])+\
            	    ','+str(self.rex.plan[i][1][2])+','+str(self.rex.plan[i][1][3])+\
            	    ','+str(end_pos[0])+','+str(end_pos[1])+','+str(end_pos[2])+ ',' +str(t_plan)+'\n')
            
        fPlan.close()
        
    	self.rex.plan_status=1
    	self.rex.wpt_number = 0
    	self.rex.oldTime = time.time()
    	
    	
    	
    	
    	
        print "Playback"

    def def_template(self):
    	    
    	#self.video.currentFrame
    	self.template_coord =np.float32([[0.0,0.0],[0.0,0.0]])
    	self.mouse_id = 0
    	self.template_flag = 1
    	    
    	
    	"""
    	pose = [200,-180,150,-0*D2R]
    	print self.rex.rexarm_IK(pose,1)
    	print self.rex.rexarm_IK(pose,0)
    	self.rex.joint_angles = self.rex.rexarm_IK(pose,0)
    	self.rex.cmd_publish()
    	#self.rex.plan = [[0,self.rex.rexarm_IK(pose,1)],[3,self.rex.rexarm_IK(pose,0)]]
    	#print self.rex.plan
    	#self.rex.wpt_total = 2
        """
    	
        print "Define Template"

    def template_match(self):
    	
    	t_start = time.time()
    	    
    	capture = Image.fromarray(self.video.currentFrame)
        capture.save("capture.jpg")
        
        """
        img_rgb = cv2.imread("capture.jpg")
        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
        template = cv2.imread("template.jpg", 0)
        
        
        w, h = template.shape[::-1]
        
        res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
        threshold = 0.8
        
        loc = np.where( res >= threshold)
        #print loc
        """
        img = cv2.imread('capture.jpg')
        img_g = cv2.imread('capture.jpg',0)
        template = cv2.imread('template.jpg',0)

        template = template - np.mean(template)

        img_r = cv2.resize(img_g, (0,0), fx=0.25, fy=0.25) 
        template_r = cv2.resize(template, (0,0), fx = 0.25, fy = 0.25)

        h,w = template_r.shape

        m,n = img_r.shape
        error = np.zeros((m-h,n-w))
        for i in range(m-h):
            for j in range(n-w):
                subimg = np.array(img_r[i:i+h,j:j+w])
                subimg = subimg - np.mean(subimg)
        
                error[i][j] = np.sum((subimg - np.array(template_r))**2)/np.sqrt((1+np.sum(subimg**2))*np.sum(np.array(template_r)**2))
        
        #error2 = error/np.max(error)
        error2 = error

        loc = np.where( error2 < 0.55)
        loc = (4*loc[0],4*loc[1])
        
        
        """draw rectangles"""
        """
        for pt in zip(*loc[::-1]):
            cv2.rectangle(img, pt, (pt[0] + 4*w, pt[1] + 4*h), (0,0,255), 2)
   
        cv2.imwrite('res1.png',img)   
        """
        t_total = time.time()-t_start
        
        
        
        prev_pt = np.array([0,0])
        
        cluster_total = 0
        cluster_nb = 0
        num_pts = np.array([0])
        x_sum_pts = np.array([0.0])
        y_sum_pts = np.array([0.0])
        locations = []
        
        for pt in zip(*loc[::-1]):
            cluster_nb_found = 0    	
            if cluster_total > 0:
            	cluster_total_temp = deepcopy(cluster_total)
            	for i in range(cluster_total_temp):
            	    if(np.linalg.norm(np.array([x_sum_pts[i],y_sum_pts[i]])/num_pts[i] - np.array(pt)) < 15):
            	        cluster_nb = i
            	        cluster_nb_found = 1
            	               	        
                if(cluster_nb_found == 0):
                    cluster_total += 1
                    cluster_nb = cluster_total - 1
                    x_sum_pts = np.append(x_sum_pts, 0)
                    y_sum_pts = np.append(y_sum_pts, 0)
                    num_pts = np.append(num_pts, 0)
            	         
            	   
            else:
                cluster_total += 1
                cluster_nb = 0
            print cluster_nb    
            x_sum_pts[cluster_nb] += pt[0]
            y_sum_pts[cluster_nb] += pt[1]
            num_pts[cluster_nb] += 1
                
        self.ui.rdoutStatus.setText("Template matching Done! Time taken: %fs. Items found: %d." 
                                    %(t_total,cluster_total))
        
        for i in range(cluster_total):
            pt_loc = np.array([x_sum_pts[i],y_sum_pts[i]])/(2*num_pts[i]) + np.array([w,h])
            locations.append(pt_loc.tolist())
            
            cv2.circle(img, (int(2*pt_loc[0]),int(2*pt_loc[1])), 2, (0,0,255), 2)
            
        cv2.imwrite('res_center.png',img) 
        
        locations2 = np.array([[pt[0],pt[1],1] for pt in locations])
        self.locations = np.transpose(locations2)
        
        
        
        print locations 
        
        for pt in zip(*loc[::-1]):
            #print pt
            cv2.rectangle(img, pt, (pt[0] + 4*w, pt[1] + 4*h), (0,0,255) ,2)
        cv2.imwrite('res2.jpg',img)
        
        
        world_coord = np.dot(self.video.aff_matrix, self.locations)
        
        N = np.shape(world_coord)[1]
        world_coord += np.vstack((np.zeros((2,N)),10*np.ones((1,N))))
        
        world_coord = np.transpose(world_coord)
        print world_coord
        
        self.rex.plan = []
        self.rex.wpt_total = 0
        t = 0
        for pt in world_coord.tolist():
            #pt1 = pt.tolist()
            print pt
            #print pt + [90]
            r = np.sqrt(pt[0]**2 + pt[1]**2)
            if (r < 115):
                phi = 115
            elif ( r  > 200):
            	phi = 45
            else:
            	phi = 85
            joint_angles = self.rex.rexarm_IK(pt + [phi],1)
            if (joint_angles==None):
                continue
            self.rex.plan.append([0, joint_angles])
            self.rex.wpt_total += 1
        fcn = lambda x: x[1][0]
        self.rex.plan = sorted(self.rex.plan,key = fcn)
        for i in range(len(self.rex.plan)):
            self.rex.plan[i][0] = t
            t += 2
        
        
        print "Template Match"

    def exec_path(self):
    	self.exec_status = not self.exec_status
        self.t_0 = time.time()
        
        
    	if self.exec_status:
    	    print "Execute Path"
    	else:
    	    print "Stop Executing Path"
        
    def exec_path_act(self):
    	    
    	if self.exec_status:
            
            """for fun path"""
    	    t = time.time()-self.t_0
    	    # t = 0.0
    	    x = 40*np.cos(0.5*t)+255
    	    y = 30*np.sin(4*t)
    	    z = 85 + 30*np.cos(4*t)
    	    phi = 0.0
    	    pose = [x,y,z,phi]
    	    self.rex.joint_angles = self.rex.rexarm_IK(pose,1)
    	    
    	    fPath = open('fun_path_joint_angles.txt','a')
            fPath.write(str(self.rex.joint_angles[0])+','+str(self.rex.joint_angles[1])+','+\
            	    str(self.rex.joint_angles[2])+','+str(self.rex.joint_angles[3])+','+\
            	    str(self.rex.joint_angles_fb[0])+','+str(self.rex.joint_angles_fb[1])+','+\
            	    str(self.rex.joint_angles_fb[2])+','+str(self.rex.joint_angles_fb[3])+','+str(t)+'\n')
            fPath.close()
            
    	    end_pos = self.rex.rexarm_FK2(self.rex.dh_table,4,self.rex.joint_angles_fb)
            fPath_end = open('fun_path_end_effector.txt','a')
            fPath_end.write(str(x)+','+str(y)+','+str(z)+','+\
            	    str(end_pos[0])+','+str(end_pos[1])+','+str(end_pos[2])+','+str(t)+'\n')
            fPath_end.close()
            
            
    	    
    	    """just for test"""
    	    #self.rex.joint_angles = (D2R * np.array([220,0,0,0])).tolist()
    	    self.rex.cmd_publish()
        
 
def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
